from entities import Trainee, Room, Instructor
from values import API_ADDRESS, API_AUTH
from utils import fetch_xml, week, parse_date, trymap
from lxml import objectify
from datetime import date, datetime, timedelta


class Week():
    """Represents a Week for a given entity (trainees, instructors or rooms)

    idt: Unique ID of the week this year (0-based from first monday of sept)
    days: List of Day object (help(Day) for more information)
    """

    def __init__(self, idt, days):
        """Should not be accessed outside of the Week object internals

        The user should use the static method Week.fetch() instead, which
        will create the Week object(s) for him
        """

        self.idt = idt
        self.days = [Day(day) for day in days]

    @staticmethod
    def fetch(ent, day=date.today(), num=1, auth=API_AUTH):
        """Fetch a list of Week objects from the HTTP API

        ent: Entity object defining which entity's planning we're looking for
        day: Select the week containing the day described by the date object
        num: The number of weeks to fetch
        auth: The auth needed to use the API

        returns: List of 'num' Week objects
        raises: Various errors if API isn't answering according to 'spec'
        """

        params = {'group': ent, 'week': week(day), 'num': num, 'auth': auth}
        xml = fetch_xml(API_ADDRESS, params)
        return Week.__explore__(objectify.fromstring(xml))

    @staticmethod
    def __explore__(tree):
        """Should not be accessed outside of the Week object internals"""

        if hasattr(tree, 'week'):
            return [Week.__explore__(week) for week in tree.week]
        elif hasattr(tree, 'day'):
            return Week(int(tree.id), tree.day)


class Day():
    """Represents a Day for a given entity (trainees, instructors or rooms)

    idt: Unique ID of the day this week (0-based from monday)
    date: datetime.date object representing the year, month and day
    events: List of Event objects (help(Event) for more information)
    """

    def __init__(self, xmltree):
        """Should not be accessed outside of the Day object internals

        The user should use the static method Day.fetch() instead, which
        will create the Day object(s) for him
        """

        self.idt = int(xmltree.id)
        self.date = parse_date(str(xmltree.date))

        self.events = []
        if hasattr(xmltree, 'course'):
            self.events = [Event(self.date, ev) for ev in xmltree.course]

    @staticmethod
    def fetch(ent, day=date.today(), num=1, auth=API_AUTH):
        """Fetch a list of Day objects from the HTTP API

        ent: Entity object defining which entity's planning we're looking for
        day: Select the day depending on this date object
        num: The number of days to fetch from the starting date
        auth: The auth needed to use the API

        returns: List of 'num' Day objects
        raises: Various errors if API isn't answering according to 'spec'
        """

        output = []
        for week in Week.fetch(ent, day, 1 + int(num / 7), auth):
            for d in week.days:
                if d.date >= day and d.date < day + timedelta(days=num):
                    output.append(d)
        return output


class Event():
    """Represents an Event for a given entity (trainees, instructors or rooms)

    idt: Unique ID of the event (unknown base or source)
    name: Name of the event (usually a course)
    time: Exact time at which the event is _supposed_ to begin (wink wink)
    dur: Duration of the event

    instructors: List of instructor names (strings)
    rooms: List of room names (strings)
    trainees: List of trainees (strings)
    """

    def __init__(self, dt, xml):
        """Should not be accessed outside of the Event object internals

        The user should use the static method Event.fetch() instead, which
        will create the Event object(s) for him
        """

        self.idt = int(xml.id)
        self.name = str(xml.title)

        self.time = datetime(dt.year, dt.month, dt.day,
                int(xml.hour) // 4, (int(xml.hour) % 4) * 15)
        self.dur = timedelta(minutes=int(xml.duration) * 15)

        for cat in ['instructor', 'room', 'trainee']:
            setattr(self, cat + 's', trymap(str, xml, cat))

    @staticmethod
    def fetch(ent, off=datetime.now(), tl=timedelta(hours=1), auth=API_AUTH):
        """Fetch a list of Event objects from the HTTP API

        ent: Entity object defining which entity's planning we're looking for
        off: datetime.datetime object representing the exact time to search from
        tl: datetime.timedelta object representing the time frame to search in
        auth: The auth needed to use the API
        """

        off = datetime.now() if not off else off
        seconds = (tl + timedelta(hours=off.time().hour)).total_seconds()
        daycount = 1 + int(seconds / 86400)

        output = []
        for day in Day.fetch(ent, off.date(), daycount, auth):
            for ev in day.events:
                if ev.time >= off and ev.time < off + tl:
                    output.append(ev)
        return output
