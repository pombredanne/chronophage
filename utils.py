from urllib import parse, request
from datetime import date


def fetch_xml(url, params, headers={}):
    data = parse.urlencode(params).encode('ascii')
    req = request.Request(url, data=data, headers=headers)
    return request.urlopen(req).read()


def flatten(ls):
    out = []
    for item in ls:
        if isinstance(item, list):
            out.extend(flatten(item))
        else:
            out.append(item)
    return out


def week(day):
    day1 = 8 - date(date.today().year - 1, 9, 1).weekday()
    week1 = date(date.today().year - 1, 9, day1).isocalendar()[1]
    return 52 - week1 + day.isocalendar()[1]


def parse_date(string):
    split = string.split(' ')[0].split('/')
    return date(int(split[2]), int(split[1]), int(split[0]))


def trymap(func, obj, field):
    return map(func, getattr(obj, field)) if hasattr(obj, field) else []
